#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Sharepy - A Python url shortener using Flask, Jinja2 and SQLAlchemy

version: 0.0.1 
author: drivedric <drivedric@gmail.com>
website: http://sharepy.drivedric.com/

You can fork this project at : https://bitbucket.org/drivedric/sharepy/fork 

"""

import urllib, random, string, datetime
from flask import Flask, request, make_response, redirect, url_for, abort, render_template 
from db.databaseManager import db, Shortener

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET': # check if form is sent
        return render_template('send_form.tpl') # and return the form if not
    
    data = request.form # a shortcut!

    # check if one or some of the fields
    # is empty. If a field is empty, <error>
    # contain the name of the empty field,
    # and an error message are return
    # else, nothing to do.
    error = None
    for key, value in data.items():
        if value == '':
            error = key + ' is empty'
            break
        else:
            continue


    if error is not None: # one or some of the fields are empty  
        return render_template('error_form.tpl', error=error) 

    # check if <url> is a real url
    # if urllib raise an IOError, then the url is invalid
    # so we raise an error 
    try:
        urllib.urlopen(data['url'])
    except IOError:
        return render_template('error_form.tpl', error='URL is invalid') 
    

    # generate the key for the url, see keygen() 
    url_key = keygen()

    # If you're here, there is no problem, so put data in db
    shortened_url = Shortener(url_key, data['url'], data['user'])
    db.session.add(shortened_url)
    db.session.commit()

    # finally, return the url to the original url
    return render_template('success.tpl', key=url_key)



@app.route('/<key>')
def redirector(key):
    """ Redirecting to the <key>'s associated url """ 

    redirect_to = Shortener.query.filter_by(url_key=key).first_or_404() # select data and return 404 if <key> doesn't exist
    redirect_to.stats += 1 # how many times my url was used?
    redirect_to.last_use = datetime.datetime.now().strftime('%Y-%m-%d %H:%M') # updating the last-used time 
    db.session.commit() # updating db
    
    return redirect(redirect_to.url_origin)

 
     
@app.route('/user/<user>')
def user(user):
    """ Show the list of the user's shortened urls """
    
    user_urls = Shortener.query.filter_by(user=user).all() 
    if user_urls == []:
        abort(404)
    return render_template('user.tpl', user_urls=user_urls, user=user)     


def keygen(n=6):
    """ Generate a key for url shortener """

    while True:
        key = ''.join(random.choice(string.letters + string.digits) for _ in range(n)) # generate a key of 6 characters (a-zA-Z1-6)
        check_key = Shortener.query.filter_by(url_key=key).first() # select data in db where the key is the key previously created
        if check_key is None: # the key doesn't exist
           return key
        else: # key exist, generate another key 
            continue



if __name__ == "__main__":
    app.run(debug=True)

