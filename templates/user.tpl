<h3>Shortened urls of {{ user }}</h3>
    <ul>
        {% for url in user_urls %}
            <li>
               shortened url <a href="/{{ url.url_key }}">/{{ url.url_key }}</a> 
               with redirect to <a href="{{ url.url_origin }}">{{ url.url_origin }}</a> 
              <br />
                 <p>This url has been seen {{ url.stats }} times, last time at {{ url.last_use }}</p>
             </li>
        {% endfor %} 
    </ul>
