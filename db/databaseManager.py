"""

Module databaseManager
   

"""


import datetime
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/drivedric/python/sharepy/db/db.sql'

db = SQLAlchemy(app)

now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')

class Shortener(db.Model):
    id         = db.Column(db.Integer, primary_key=True, unique=True)
    url_key    = db.Column(db.String(6), unique = True) 
    url_origin = db.Column(db.String)
    user       = db.Column(db.String(80))
    stats      = db.Column(db.Integer)
    last_use   = db.Column(db.String)

    def __init__(self, url_key, url_origin, user, stats=1, last_use=now):
        self.url_key     = url_key
        self.url_origin  = url_origin
        self.user        = user
        self.stats       = stats
        self.last_use    = last_use
